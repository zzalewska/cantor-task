let usd_url = 'https://api.nbp.pl/api/exchangerates/rates/a/usd/?format=json';
let output = document.getElementById('output');
let alert_span = document.getElementById('alert_span');
let pln = document.getElementById('pln');
let gbp = document.getElementById('gbp');
// gbp.defaultValue = window.location.href.substring(91);

run();

async function getCurrency() {
  let response = await fetch(usd_url);
  let json = await response.json();
  return json.rates[0].mid;
}
async function run() {
  let x = await getCurrency();
  output.innerHTML = "1 GBP = <b>" + x.toFixed(2) + " PLN</b>";

  gbp.oninput = function () {
      pln.value = (parseFloat(gbp.value)*x).toFixed(2);

      if(gbp.value==""){
        pln.value="";
      }
  };

  pln.oninput = function () {
      gbp.value = (parseFloat(pln.value)*(1/x)).toFixed(2);

      if(pln.value==""){
        gbp.value="";
      }
  };
}

function decimals(that) {
  var s = that.value;
  var i = s.indexOf(".");
  if (i < 0 || s.substr(i+1).length < 3) return;
  alert_span.innerHTML="Only <b>2</b> digit to the right of the decimal are allowed!<br>";
  that.value = s.substring(0,i+3);
  }
