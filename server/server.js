var express = require('express');
var app = express();
const liveReload = require('livereload');
const publicPath = __dirname + '/../web-client/';
const bodyParser = require('body-parser');
let reload;

if (process.env.NODE_ENV !== 'production') {
  const liveReloadMiddleware = require('connect-livereload')({
    port: 35729
  });
  reload = liveReload.createServer({
    exts: ['json', 'html', 'css']
  });
  app.use(liveReloadMiddleware);
}

// app.use(
//   bodyParser.urlencoded({
//     extended: true
//   })
// );
app.use(bodyParser.json());

app.use(express.static(publicPath));

app.get('/', (req, res) => res.sendFile('index.html', { root: publicPath }));


app.listen(process.env.PORT || 5000, () => console.log(`Server is started`));
if (process.env.NODE_ENV !== 'production') {
  reload.watch(publicPath);
}
